<?php
include "templates.php";
outputHeaders("Home | Andrew Perrin");
?>
	<header id="header">      
        <div class="container">
            <div class="row">
                <div class="col-sm-12 overflow">
                   <div class="social-icons pull-right">
                        <ul class="nav nav-pills">
                            <li><a href="https://twitter.com/andrewperrindev"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://www.linkedin.com/in/andrewperrindev"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div> 
                </div>
             </div>
        </div>
        <div class="navbar" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown active"><a href="/">Home</a></li>
                        <li class="dropdown">
                        	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Projects <i class="fa fa-angle-down"></i></a>
                            <ul role="menu" class="dropdown-menu">
	                            <li><a href="/#/projects">Overview</a></li>
	                            <li role="separator" class="divider"></li>
	                            <li><a href="/#/projects/career">Healthcare</a></li>
                                <li><a href="/#/projects/projectnom">ProjectNom</a></li>
                                <li><a href="/#/projects/trowl">Trowl</a></li>
                                <li><a href="/#/projects/bots">Twitter Bots</a></li>
                            </ul>
                        </li>
                        <li class="dropdown"><a href="/blog">Blog</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!--/#header-->

    <section id="app">
	    <noscript>
	    	<div class="container">
		    	<div class="row">
			    	<div class="col-xs-offset-1 col-xs-10 col-sm-offset-2 col-sm-9 col-md-offset-2 col-md-8">
				    	<h1>Hello</h1>
				    	<p>It appears you don't have JavaScript enabled. At this time, Javascript is required to view this site.</p>
				    	<p>If you don't have a Javascript-enabled browser, feel free to send me a message with any questions you may have -- just use the form below. Thanks!</p>
			    	</div>
		    	</div>
	    	</div>
	    </noscript>
    </section>
    <!--/#app-->


    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="contact-info bottom">
                        <h2>Contacts</h2>
                        <address>
                        E-mail: <span id="email-link"></span><noscript>Please use contact form</noscript><br>
                        LinkedIn: <a href="https://www.linkedin.com/in/andrewperrindev">Andrew Perrin</a><br>
                        Twitter: <a href="https://twitter.com/andrewperrindev">@andrewperrindev</a><br>
                        </address>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="contact-form bottom">
                        <h2>Send a message</h2>
                        <form id="main-contact-form" name="contact-form" method="post" action="sendemail.php">
                            <div class="form-group">
                                <input type="text" name="name" id="form-name" class="form-control" required="required" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <input type="email" name="email" id="form-email" class="form-control" required="required" placeholder="E-mail">
                            </div>
                            <div class="form-group" id="form-url">
                                <input type="text" name="url" placeholder="Leave this field blank">
                            </div>
                            <div class="form-group">
                                <textarea name="message" id="form-message" required="required" class="form-control" rows="8" placeholder="Your message here"></textarea>
                            </div>                        
                            <div class="form-group submit">
                                <input type="submit" id="form-submit" name="submit" class="btn btn-submit" value="Submit">
								<div id="email-success" class="alert alert-success" role="alert"><strong>Thanks!</strong> Your message has been sent.</div>
								<div id="email-error" class="alert alert-danger" role="alert">There was a problem sending your message. Please make sure all fields are filled in.</div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="copyright-text text-center">
                        <p>&copy; Andrew Perrin.</p>
                        <p>Designed by <a target="_blank" href="http://www.themeum.com">Themeum</a></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/#footer-->

    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="<?php file_exists("js/exports.js") ? print "js/exports.js" : print "js/exports.min.js";?>"></script>
    <script src="<?php file_exists("js/app.js") ? print "js/app.js" : print "js/app.min.js";?>"></script>
<?php
outputFooters();
?>