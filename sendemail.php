<?php 
include "templates.php";
$return = array("success" => true, "message" => "OK");

if (isset($_POST['json'])) 
{
	$formData = json_decode($_POST['json'], TRUE);
	$name = trim($formData['name']);
	$email = trim($formData['email']);
	$message = trim($formData['message']);
	$url = $formData['url'];
}
else
{
	$name = trim($_POST['name']);
	$email = trim($_POST['email']);
	$message = trim($_POST['message']);
	$url = $_POST['url'];
}

// if the url field is empty 
if (empty($url))
{
	if (empty($name) || empty($message))
	{
		$return['success'] = false;
		$return['message'] = "Some fields were empty.";
	}
	else
	{
	    // put your email address here     
	    $destination = "hello@andrewperr.in";
	
	    // prepare a "pretty" version of the message
	    $body = "Contact form at andrewperr.in was submitted:\n\nFrom:  $name <$email>\n$message"; 
	
	    // Use the submitters email if they supplied one     
	    // (and it isn't trying to hack your form).     
	    // Otherwise send from your email address.     
	
	    if(!empty($email) && !preg_match("/[\r\n]/", $email)) 
	    {
	        $headers = "Reply-To: $email";
	    } 
	    else 
	    {
	        $headers = "Reply-To: $destination";
	    }
	    
	    $headers .= "\r\nFrom: Hello AndrewPerr.in <hello@andrewperr.in>";
	
	    // finally, send the message
		if (!mail($destination, 'Contact Form Submission', $body, $headers))
		{
			$return['success'] = false;
			$return['message'] = "Email server returned error.";
		}
	}
} // otherwise, let the spammer think that they got their message through

if (isset($formData))
{
	header("Content-Type: application/json");
	print json_encode($return);
}
else
{
	outputHeaders("Contact | Andrew Perrin");
	if ($return['success'])
	{
		?>
		<center>
			<h1>Thank You</h1>
			<p>Your message has been sent.</p>
			<p><a href="/">Return to home</a></p>
		</center>
		<?php
	}
	else
	{
		?>
		<center>
			<h1>Apologies</h1>
			<p>An error occurred. Please try again later.</p>
			<p><a href="/">Return to home</a></p>
		</center>
		<?php
	}
	outputFooters();
}
?>