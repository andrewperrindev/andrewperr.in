"use strict";

import React from 'react';
import ReactDOM from 'react-dom';

import App from './components/App.jsx';

$(function() {

	// Split location into `/` separated parts, then render `App` with it
	function handleNewHash() {
		var location = window.location.hash.replace(/^#\/?|\/$/g, '').split('/');
		var application = <App location={location} />;
		ReactDOM.render(application, document.getElementById('app'));
	}
	
	// Handle the initial route and browser navigation events
	handleNewHash();
	window.addEventListener('hashchange', handleNewHash, false);
	
	// Dynamically build email in an effort to thwart spambots.
	let first = "hello";
	let last = "andrewperr.in";
	$("#email-link").html("<a href=\"mailto:" + first + "@" + last + "\">" + first + "@" + last + "<\/a>");
	
	$("#main-contact-form").submit(function onSubmit(event) {
		event.stopPropagation();
		event.preventDefault();
		
		$("#email-error").hide();
		$("#form-submit").prop("disabled", true);
		
		var formData = {
			name: $("#form-name").val(),
			url: $("#form-url input").val(),
			email: $("#form-email").val(),
			message: $("#form-message").val()
		};
		
		$.post("sendemail.php", "json=" + JSON.stringify(formData), "json")
			.done(function(data) {
				if (data.success) {
					$("#form-submit").hide();
					$("#email-success").show();
				}
				else {
					$("#email-error").show();
					$("#form-submit").prop("disabled", false);
				}
			})
			.fail(function(data) {
				$("#email-error").show();
				$("#form-submit").prop("disabled", false);
			});
	});

});