"use strict";

import React from 'react';

import Home from './Home.jsx';
import ItemBlockGroup from './ItemBlockGroup.jsx';
import ProjectDetail from './ProjectDetail.jsx';

class App extends React.Component {
	constructor(props) {
		super(props);
		
		this.titleSuffix = " | Andrew Perrin";
	}
	
	setTitle(newTitle) {
		document.title = newTitle + this.titleSuffix;
	}
	
	componentDidUpdate() {
		window.scrollTo(0,0);
		$('.navbar-collapse').collapse('hide');
	}
	
	render() {
		switch (this.props.location[0])  {
		case '':
			this.setTitle("Home");
			return <Home />;
			
		case 'projects':
			switch (this.props.location[1]) {
				case 'projectnom':
					this.setTitle("ProjectNom");
					return <ProjectDetail item="ProjectNom" />;

				case 'trowl':
					this.setTitle("Trowl");
					return <ProjectDetail item="Trowl" />;

				case 'bots':
					this.setTitle("Twitter Bots");
					return <ProjectDetail item="Bots" />;
					
				case 'career':
					this.setTitle("Career");
					return <ProjectDetail item="Career" />;
				
				default:
					this.setTitle("Projects");
					return <ItemBlockGroup dataFile="projects" />;
			}

		default:
			this.setTitle("Error");
			return <div><h1>Not Found</h1></div>;
		}
	}
}

export default App;