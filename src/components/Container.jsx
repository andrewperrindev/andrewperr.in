"use strict";

import React from 'react';

class Container extends React.Component {
	constructor(props) {
		super(props);
		
		this.classNames = "container";
		if (this.props.flush == "left") {
			this.classNames += " container-flushleft";
		}
	}

	lockHeight() {
		$(this.divContainer).height($(this.divContainer).height());
	}
	
	unlockHeight() {
		$(this.divContainer).height("");
	}
	
	render() {
	    return (
	      <div 
	      	className={this.classNames} 
	      	style={{minHeight: "250px"}}
	      	ref={(container) => { this.divContainer = container; }}>
	      		{this.props.children}
	      </div>
	    );
	}
}

export default Container;