"use strict";

import React from 'react';

import Container from './Container.jsx';
import Row from './Row.jsx';
import Heading from './SectionHeading.jsx';
import Intro from './Intro.jsx';
import ItemBlockGroup from './ItemBlockGroup.jsx';

class Home extends React.Component {
	constructor(props) {
		super(props);
	}
	
	render() {
		return (
			<div>
		    	<Container flush="left">
					<Row style={{marginBottom: '50px'}}>
		      			<Intro />
			  		</Row>
			  	</Container>
			  	<Container>
			  		<Row>
			  			<Heading>More About Me</Heading>
			  		</Row>
			  		<ItemBlockGroup dataFile="about" />
			  	</Container>
			</div>
		);
	}
}

export default Home;