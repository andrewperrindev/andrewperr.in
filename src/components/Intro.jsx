"use strict";

import React from 'react';

class Intro extends React.Component {
	constructor(props) {
		super(props);
	}
	
	render() {
	    return (
		    <div>
		    	<div className="hidden-xs hidden-sm col-md-7 col-lg-6">
			        <img src="images/profile-full.jpg" className="img-responsive nogutter" />
			    </div>
	            <div className="col-xs-offset-3 col-xs-6 col-sm-offset-4 col-sm-4 visible-xs-block visible-sm-block profile-img">
	                <img src="images/profile.png" className="img-responsive" />
	            </div>
	            <div className="col-xs-offset-2 col-xs-8 col-md-offset-0 col-md-5">
	                <h1>Andrew Perrin</h1>
	                <p>Good software solves a problem. Great software is elegant and performs well. Excellent software is maintainable, secure and backed by a suite of automated tests. I'm a software engineer that aims for that highest bar in all projects I take on. Professionally, I engineer solutions for the healthcare industry, where there is no shortage of challenging problems to solve.</p>
	            </div>
            </div>
	    );
	}
}

export default Intro;