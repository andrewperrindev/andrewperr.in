"use strict";

import React from 'react';

class ItemBlock extends React.Component {
	constructor(props) {
		super(props);
	}
	
	render() {
	    return (
	      <div className="col-xs-offset-2 col-xs-8 col-sm-offset-0 col-sm-6 col-md-4 text-center item-block">{this.props.children}</div>
	    );
	}
}

export default ItemBlock;