"use strict";

import React from 'react';

import Row from './Row.jsx';
import Container from './Container.jsx';
import ItemBlock from './ItemBlock.jsx';
import Spinner from 'react-spinkit';

class ItemBlockGroup extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			items: []
		};
	}

	fetchData() {
		let self = this;

		if(this.props.dataFile)
		{
			$.get("/data/" + this.props.dataFile + ".json")
				.done(function process(data) {
					self.setState({
						items: data
					});
				});
		}
	}

	componentDidMount() {
		this.fetchData();
	}

	render() {
	    return (
		    <Container>
	            <Row>
	            	{
		            	this.state.items.length
							? this.state.items.map(function(item, i) {
					            return (<div key={i}>
						            <ItemBlock>
						            	{item.image &&
											<img src={item.image} style={{paddingBottom: "10px"}} />
										}
										{item.fa &&
											<h1><i className={"fa fa-" + item.fa}></i></h1>
										}
						            	<div className="caption">
						            		<p>{item.description}</p>
						            		{item.url &&
												<p><a href={item.url}>Read More</a></p>
											}
						            	</div>
						            </ItemBlock>
						            {(i+1) % 2 === 0 &&
										<div className="clearfix visible-sm-block"></div>
						            }
								</div>
					            );
			            	})
				    		: <div className="col-xs-1 col-xs-offset-6" style={{left: "-25px"}}>
				    			<Spinner spinnerName="wave" />
				    		</div>
	            	}
	            </Row>
            </Container>
		);
	}
}

export default ItemBlockGroup;