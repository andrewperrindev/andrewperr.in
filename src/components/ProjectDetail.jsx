"use strict";

import React from 'react';

import Row from './Row.jsx';
import Container from './Container.jsx';
import SectionHeading from './SectionHeading.jsx';
import ReactMarkdown from 'react-markdown';
import Spinner from 'react-spinkit';

class ProjectDetail extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			image: "",
			fa: "",
			title: "",
			content: ""
		};
	}
	
	fetchData(item) {
		let self = this;
		let dataFile = "";
		
		if (item == "ProjectNom") {
			dataFile = "projectnom.json";
		}
		else if (item == "Trowl") {
			dataFile = "trowl.json";
		}
		else if (item == "Bots") {
			dataFile = "bots.json";
		}
		else if (item == "Career") {
			dataFile = "career.json";
		}


		$.get("/data/" + dataFile)
			.done(function process(data) {
				self.setState({
					image: data.image,
					fa: data.fa,
					title: data.title,
					content: data.content
				});
			});
	}
	
	componentDidMount() {
		this.fetchData(this.props.item);
	}
	
	componentWillReceiveProps(newProps) {
		this.detailContainer.lockHeight();
		
		this.state = {
			title: ""
		};
		
		this.fetchData(newProps.item);
	}
	
	componentDidUpdate() {
		if (this.state.title) {
			this.detailContainer.unlockHeight();
		}
	}
	
	render() {
	    return (
		    <Container
		    	ref={(container) => { this.detailContainer = container; }}>
		    	{(!this.state.title)
			    	&& <Row>
		    			<div className="col-xs-1 col-xs-offset-6" style={{left: "-25px"}}>
		    				<Spinner spinnerName="wave" />
						</div>
					</Row>
		    	}
		    	{(this.state.title && this.state.image)
			    	&& <Row>
		    			<div className="col-xs-12 text-center">
							<img src={this.state.image} />
						</div>
					</Row>
		    	}
		    	{(this.state.title && this.state.fa) 
			    	&& <Row>
			    		<div className="col-xs-12 text-center">
							<h1><i className={"fa fa-" + this.state.fa}></i></h1>
						</div>
					</Row>
				}
		    	{(this.state.title)
			    	&& <Row>
	            		<SectionHeading>{this.state.title}</SectionHeading>
					</Row>
		    	}
	            {(this.state.title && this.state.content)
		            && <Row>
						<ReactMarkdown source={this.state.content} skipHtml={true} 
							className="col-xs-10 col-xs-offset-1" />
					</Row>
	            }
            </Container>
	    );
	}
}

export default ProjectDetail;