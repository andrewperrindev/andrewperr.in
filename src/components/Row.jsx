"use strict";

import React from 'react';

class Row extends React.Component {
	constructor(props) {
		super(props);
		
		this.rowStyle = {paddingTop: '10px', paddingBottom: '10px'};
		this.customStyle = this.props.style || {};
		
		Object.assign(this.rowStyle, this.customStyle);
	}
	
	render() {
	    return (
	      <div className="row" style={this.rowStyle}>{this.props.children}</div>
	    );
	}
}

export default Row;