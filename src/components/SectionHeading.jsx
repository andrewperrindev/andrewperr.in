"use strict";

import React from 'react';

class SectionHeading extends React.Component {
	constructor(props) {
		super(props);
	}
	
	render() {
	    return (
            <div className="col-xs-offset-3 col-xs-6 text-center">
	            <h1>{this.props.children}</h1>
            </div>
	    );
	}
}

export default SectionHeading;