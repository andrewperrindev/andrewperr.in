<?php
function outputHeaders($title)
{
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Andrew Perrin: Software Engineer, Cook & Writer. Want to learn more?">
    <meta name="author" content="Andrew Perrin">
    <title><?php print $title; ?></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php file_exists("css/main.css") ? print "css/main.css" : print "css/main.min.css";?>" rel="stylesheet">

</head><!--/head-->

<body>
<?php
}

function outputFooters()
{
?>
</body>
</html>
<?php
}
?>